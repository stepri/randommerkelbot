var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var morgan = require('morgan');
var core = require('./core');

var app = express();
app.use(morgan(':date[web] - :method :url :status :res[content-length] - :response-time ms'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.disable('x-powered-by');

app.get('/', core.root);
app.post('/webhook/telegram', core.telegram);

app.use(function(req, res, next) {
  res.status(404).send();
});

app.listen(process.env.PORT || 80, function(){
  console.log('Server started at ' + (process.env.PORT || 80));
});
