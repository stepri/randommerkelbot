var request = require('request');
var fs = require('fs');

var stickers = [
  'BQADBAADyAADaFTuAxaKGGEuNNyhAg',
  'BQADAgADcQADOQ-GAwywU8xtJGSDAg',
  'BQADBAADlAADaFTuA5eIkIN04ilHAg',
  'BQADBAADrwADaFTuA_1XfSzovmOcAg',
  'BQADAgADgQADOQ-GA9_aULN1YBr5Ag',
  'BQADAgADiQADOQ-GA0DilIN1fXh_Ag',
  'BQADAgADgwADOQ-GA9uZYeYEg82WAg',
  'BQADBAADZgEAAgkyvwAB69mqqSrj1skC',
  'BQADAgADhQADOQ-GAxbCpXBPb9zQAg',
  'BQADAgADqQADOQ-GAzgjzBX6_qs2Ag',
  'BQADBAADtgADaFTuAxTWfHJXVWDvAg',
  'BQADBAADEwEAAmhU7gP7aSknptjA9gI',
  'BQADBAAD7gADaFTuA05d5BRzm8dwAg',
  'BQADAgADlQADOQ-GA09zU_nMwWmhAg',
  'BQADAgADCwEAAjkPhgN0b2fDC_h-gQI',
  'BQADAgADpQADOQ-GAyCxGrRE_LBDAg',
  'BQADAgADnQADOQ-GAxcjt239EbOxAg',
  'BQADAgADJwEAAjkPhgM-6OeVXOARnQI',
  'BQADBAAD8AADaFTuA86hqfKBQh1qAg',
  'BQADAgADQAEAAjkPhgMc8MbYkdcIOwI',
  'BQADBAADzgADaFTuA9p_G7hhSDL6Ag',
  'BQADBAADwAADaFTuA2PE5k8hmundAg'
];

var images = [
  'image1.jpg',
  'image2.jpg',
  'image3.jpg'
];

function randomIntFromInterval(min,max){
    return Math.floor(Math.random()*(max-min+1)+min);
}

exports.root = function(req, res) {
  res.send('online');
} 
exports.telegram = function(req, res) {

  var body = req.body;

  console.log('body', body);

  res.send();

  if (body && body.message) {

    if (randomIntFromInterval(1,2) === 2) {
      var sticker = stickers[Math.floor(Math.random() * stickers.length)];
      
      request.get('https://api.telegram.org/bot'+process.env.TELEGRAM_TOKEN+'/sendSticker?chat_id='+ body.message.chat.id +'&reply_to_message_id='+body.message.message_id+'&sticker='+sticker, function (error, response, body) {
        if (error) console.log(error);
        if (body) console.log(body);
        res.send();

      });  
    } else {
      var imageNumber = randomIntFromInterval(1,19)
      var formData = {
        photo: fs.createReadStream(__dirname + '/photos/image'+imageNumber+'.jpg'),
      }
      request.get({url: 'https://api.telegram.org/bot'+process.env.TELEGRAM_TOKEN+'/sendPhoto?chat_id='+ body.message.chat.id +'&reply_to_message_id='+body.message.message_id, formData: formData}, function (error, response, body) {
        if (error) console.log(error);
        if (body) console.log(body);
        res.send();

      });        
    }


  } else {
    res.send();
  }

}